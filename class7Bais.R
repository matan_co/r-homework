spam <- read.csv('spam.csv', stringsAsFactors = FALSE)
View(spam)
#��� ����� ����� �� ���� ������ �� ���� �� ����� ������� ��� ������
#������� ������ ���� ����� ���� ������ �� ����, ��� �� �����
#���� ������� �� ���� ������ ��� �����, ����
#���� �� ����� ���� ��� ����� ����� ������� ����, ��� �� ���� �� ��, ���� ������ �������
#���� �� ���� Document Term Matrix
#����� ��� ���� ������, ��� ����� �����, ������ stopwords
#����� ����� �"����" ���� ������ ��� �� ����� ��� ����� - ��� �����
#���� ��� ���� ���� ������ �� ����� ���, ��� ���� ����� �� �������
install.packages('tm')
library(tm) #textmining
install.packages('wordcloud')
library(wordcloud)
install.packages('e1071')
library(e1071) #������� ��� �� �� ��������� �� ���� �����

#����� �� �"���" ������
spam$type <- as.factor(spam$type)
str(spam)

#�������� ���� �����

#Generating DTM
spam_corpus <- Corpus(VectorSource(spam$text))
spam_corpus[[1]][[1]] #����� ������ ��� ����� 1
spam_corpus[[1]][[2]] #����� �� ����
spam_corpus[[2]][[1]] #����
spam_corpus[[2]][[2]] #����

#Cleaning the text
clean_corpus <- tm_map(spam_corpus, removePunctuation) #���� ����� ������
clean_corpus[[1]][[1]]

clean_corpus <- tm_map(clean_corpus, stripWhitespace) #������ �� ����� ���� �� ��� ������� ����

clean_corpus <- tm_map(clean_corpus, content_transformer(tolower)) #����� ������ ������ ������
clean_corpus[[1]][[1]]

stopwords() #����� ����� ���� ����� �� ������� ����� ������ ��

clean_corpus <- tm_map(clean_corpus, removeWords, stopwords())
clean_corpus[[1]][[1]]
clean_corpus <- tm_map(clean_corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean_corpus) 
dim(dtm)

#remove infreuent words
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm, 10))) #����� ������ ������ ������ ����� ������� �� ���� ��� ������� ���� ���� �10
dim(frequent_dtm)

#Data visualization
#wordcloud
pal <- brewer.pal(9, 'Dark2') #����� ��� �� �����

wordcloud(clean_corpus, min.freq = 5, random.order = F, colors = pal) #���� �������� �� ������ ������� ���� �5 �����

#spam
wordcloud(clean_corpus[spam$type == 'spam'], min.freq = 5, random.order = F, colors = pal)

#ham
wordcloud(clean_corpus[spam$type == 'ham'], min.freq = 5, random.order = F, colors = pal)

#Spliting into training set and test set
split <- runif(500) #����� ���� ������� �����, 500 ����� ����� �������

split <- split > 0.3 #����� �30-70

#dividing raw data
train_raw <- spam[split,] #raw �����
test_raw <- spam[!split,]

#dividing clean corpus
train_corpus <- clean_corpus[split]
test_corpus <- clean_corpus[!split]

#dividing dtm
train_dtm <- frequent_dtm[split,]
test_dtm <- frequent_dtm[!split,]

#Convert the DTM into yes/no
conv_yesno <- function(x){
  x <- ifelse(x > 0, 1, 0)
  x <- factor(x, levels = c(1,0), labels = c('Yes','No'))
}

train <- apply(train_dtm, MARGIN = 1:2, conv_yesno) #Marging �� �� ����� ����� ������ �� �������� ������ ����� �� �� ���, ���� ������ �� ���
test <- apply(test_dtm, MARGIN = 1:2, conv_yesno)

#converting into data frame
df_train <- as.data.frame(train)
df_test <- as.data.frame(test)

#Add the type column
df_train$type <- train_raw$type
df_test$type <- test_raw$type

dim(df_train) #type = ����� 58

#Generating the model using naive bayes
model <- naiveBayes(df_train[,-58], df_train$type) #���� ����� ��� ����� 58 �� ����� ����� ���� ����� 58

prediction <- predict(model, df_test[,-58])

install.packages('SDMTools') #Confusion matrixs
library(SDMTools)

#������� ���� ��������� �� ���� ������ �� ���� ��� ������ - 0 �1
convert_01 <- function(x){
  x <- ifelse(x == 'spam', 1, 0)
}

pred_01 <- sapply(prediction, convert_01)
actual <- sapply(df_test$type, convert_01)

confusion <- confusion.matrix(actual, pred_01) #�� ������ ����

TP <- confusion[2,2] #1,1
FP <- confusion[2,1] #1,0
TN <- confusion[1,1] #0,0
FN <- confusion[1,2] #0,1

recall <- TP/(TP+FN) #��� �� ��� ��� ������� ��� ��� ������ ����
precision <- TP/(TP+FP) #���� �� ��� ��� ������� ��� ��� ���� �������


