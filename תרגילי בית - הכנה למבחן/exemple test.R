category <- read.csv("test.csv", stringsAsFactors = F)
category$category <-as.factor(category$category)
View(category)

#-----------------------------------------------------------
#Q1 - Data preparation - generate dtm
library(tm)
#create corpus
cat_corpus <-Corpus(VectorSource(category$text)) #on text

#create dtm
dtm <- DocumentTermMatrix(cat_corpus)
dim(dtm)

#clean
clean_corpus <-tm_map(cat_corpus, removePunctuation)
clean_corpus <-tm_map(clean_corpus, content_transformer(tolower))
clean_corpus <- tm_map(clean_corpus,removeNumbers)
clean_corpus <-tm_map(clean_corpus, removeWords, stopwords())
clean_corpus <-tm_map(clean_corpus, stripWhitespace)

#new dtm
dtm<-DocumentTermMatrix(clean_corpus)
dim(dtm)

#Freq values
frequent_dtm<-DocumentTermMatrix(clean_corpus,list(dictionary=findFreqTerms(dtm,4)))
dim(frequent_dtm)

#Print 6 first rows and summary
inspect(frequent_dtm[1:6,])

#-----------------------------------------------------------
#Q2 - Naive Bayes
#convert the DTM into yes/no
conv_yesno<-function(x){
  x<-ifelse(x>0,1,0)
  x<-factor(x,levels = c(1,0), labels = c('Yes', 'No'))
}

df <- apply(frequent_dtm,MARGIN = 1:2,conv_yesno)

df <- as.data.frame(df)

df$category <- category$category #add category column

str(df)

df[,18]

#Naive Bayes
library(e1071)
model.nb <-naiveBayes(df[,-18],df$category)

print(model.nb)

#-------------------------------------------------
#Q3 - Prediction naive bayes+Confusion matrix
prediction.nb <- predict(model.nb,df[,-18])
predictionprob.nb <- predict(model.nb,df[,-18], type ='raw')

predictionprob.nb[,'economy']

#Confusion matrix
library(SDMTools)

conv_10 <- function(x){
  x <- ifelse( x =='Sport',1,0)
}

pred01_nb <- sapply(prediction.nb,conv_10)
actual01_nb <- sapply(df$category,conv_10)

#Create confusion matrix
confusion_nb <- confusion.matrix(actual01_nb, pred01_nb)

TP_nb <- confusion_nb[2,2]
FP_nb <- confusion_nb[2,1]
TN_nb <- confusion_nb[1,1]
FN_nb <- confusion_nb[1,2]

recall_nb <- TP_nb/(TP_nb+FN_nb)
precision_nb <- TP_nb/(TP_nb+FP_nb)

#-----------------------------------------------------
#Q4 - Data preparation for logistic reggression
#Missing Values
set.seed(101)
any(is.na(frequent_dtm)) #False

#Convert dtm as data frame, before convert as matrix
dtm_log <- as.matrix(frequent_dtm)
df_log <- as.data.frame(dtm_log)
  
#Add target value to df
df_log$category <- category$category

#Run logisitic model
log.model <- glm(category ~ . , family = binomial(link = 'logit'), df_log)
summary(log.model)

#----------------------------------------------
#Q5 - Confusion matrix of logisitic
predict.probabilities_log <- predict(log.model, df_log, type = 'response')
confusion_log <- table(predict.probabilities_log>0.5, df_log$category)

#-----------------------------------------------
#Q6 - Kmeans
Cluster <- kmeans(df_log[, 1:17], 2, nstart = 20)
Cluster$cluster #Results
table(Cluster$cluster, df_log$category)