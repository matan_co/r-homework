bike <- read.csv("train.csv")
str(bike)
bike$datetime <- as.character(bike$datetime)
bike$date <- sapply(strsplit(bike$datetime,' '), "[", 1)
bike$date <- as.Date(bike$date)
bike$time <- sapply(strsplit(bike$datetime,' '), "[", 2)
bike$hour <-  sapply(strsplit(bike$time,':'), "[", 1)
bike$hour <- as.numeric(bike$hour)

#ggplot2
install.packages('ggplot2')
library(ggplot2)

#Q1 - A chart showing how temperature affects the count
q1 <- ggplot(bike, aes(x = bike$temp, y = bike$count)) + geom_bin2d() + scale_fill_gradient(low = 'green', high = 'red') + ggtitle("how temperature affects the count")

#Q2 - A chart showing how date affects count
q2 <- ggplot(bike, aes(x = bike$date, y = bike$count)) + geom_point(color = 'red') + ggtitle("how date affects count")

#Q3 - A chart showing how season affects count (use here a boxplot) learn here about what a boxplot is and how to interpret it (turn season into factor first, add labels to season (hint - the factor function has a label parameter)
bike$season <- as.factor(bike$season)
bike$season <- factor(bike$season, levels = c(1,2,3,4), labels = c("spring", "summer", "fall", "winter"))
str(bike$season)
q3 <- ggplot(bike, aes(x = bike$season, y = bike$count)) + geom_boxplot() + ggtitle("how season affects count")

#Q4 - A chart showing how hour AND temperature together affects count. Use x axis for hour and color for temperature. Split the chart to weekdays and sunday. (bonus - learn how to use ggplot to show these two charts on the same page)
q4 <- ggplot(bike, aes(x = bike$hour, y = bike$count)) + geom_point(aes(color = factor(bike$temp))) + ggtitle("how hour AND temperature together affects count")
q4 <- q4 + facet_grid(. ~ bike$workingday)

#Q5 - A chart showing anything you like so long as it provides an interesting insight into the data 
#how holiday affects count
q5 <- ggplot(bike, aes(x = bike$hour, y = bike$count)) + geom_point() + ggtitle("how holiday affects count")
q5 <- q5 + facet_grid(. ~ bike$holiday)
